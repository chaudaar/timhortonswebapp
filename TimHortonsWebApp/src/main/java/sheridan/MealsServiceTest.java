package sheridan;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import sheridan.MealsService;

public class MealsServiceTest {

	MealsService meal = new MealsService();
	
List<String> drinks = meal.getAvailableMealTypes(MealType.DRINKS);
	@Test
	public void testDrinksRegular() {
		assertTrue("Testingg for regular case", drinks != null);
	}

	@Test
	public void testDrinksException() {
		assertFalse("Testing for exception case", drinks == null );
	}
	
	@Test
	public void testDrinksBoundaryIn() {
		assertTrue("Testing for boundary in case", drinks.size() >= 3 );
	}
	
	@Test
	public void testDrinksBoundaryOut() {
		assertFalse("Testing for boundary out case", drinks.size() == 0 );
	}
	
}
